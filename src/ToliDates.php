<?php
namespace tolical;

class ToliDates
{
    const SECONDS_IN_DAY = 86400;
    const SECONDS_TOGO_NEXTDAY = 90000;
    public static function nextDate($ymd){
        return date("Y-m-d",strtotime($ymd) + self::SECONDS_TOGO_NEXTDAY);
    }

    public static function previousDate($ymd){
        return date("Y-m-d",strtotime($ymd) - 7200);
    }

    public static function nextMonth($ym)
    {
        if ($ym == "") $ym = date("Y-m");
        $m = substr($ym,5,2);
        $m = (int)$m;
        $m++;
        if ($m < 13){
            if ($m<10) $m = "0" . $m;
            return substr($ym,0,4) . "-" . $m;
        }
        $y = (int)substr($ym,0,4);
        $y++;
        return $y . "-01";
    }

    public static function previousMonth($ym)
    {
        if ($ym == "") $ym = date("Y-m");
        $m = substr($ym,5,2);
        $m = (int)$m;
        $m--;
        if ($m > 0){
            if ($m<10) $m = "0" . $m;
            return substr($ym,0,4) . "-" . $m;
        }
        $y = (int)substr($ym,0,4);
        $y--;
        return $y . "-12";
    }


    public static function getDates($fromYmd,$tillYmd)
    {
        $day = $fromYmd;
        do {
            $d[]  = $day;
            $day = self::nextDate($day);
        } while ($day <= $tillYmd);
        return $d;
    }

    public static function toDmy($ymd)
    {
        return substr($ymd,5,2) . "/" . substr($ymd,8,2) . "/" . substr($ymd,0,4);
    }

    public static function getDay($ymd)
    {
        return substr($ymd,8,2);
    }

    public static function getMonth($ymd)
    {
        return substr($ymd,5,2);
    }

    public static $Months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
            //["Januari","Februari","Maart","April","Mei","Juni","Juli","Augustus","September","Oktober","November","December"];
    public static function getMonthName($ymd_or_monthnr)
    {
        if (!is_numeric($ymd_or_monthnr))
            $ymd_or_monthnr = self::getMonth ($ymd_or_monthnr);
        $ymd_or_monthnr = (int) $ymd_or_monthnr;
        return self::$Months[($ymd_or_monthnr-1)];
    }

    public static function getYear($ymd)
    {
        return substr($ymd,0,4);
    }

    public static function today()
    {
        return date("Y-m-d");
    }

    public static function yesterday()
    {
        return self::previousDate(date("Y-m-d"));
    }

    public static function dateToWeekday($ymd)
    {
        $wd = jddayofweek(cal_to_jd(
                            CAL_GREGORIAN,
                            self::getMonth($ymd),
                            self::getDay($ymd),
                            self::getYear($ymd)
                           ));
        if ($wd == 0) return 7;
        return $wd;
    }

    /**
     *
     * @param type $ym
     * @return type
     */
    public static function daysInCurrentMonth($ym="")
    {
        if ($ym == "") $ym = date("Y-m");
        //return cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
        return cal_days_in_month(CAL_GREGORIAN, substr($ym, 5,2), substr($ym,0,4));
    }

    /**
     *
     * @param type $ym
     * @return type
     */
    public static function lastDateOfCurrentMonth($ym="")
    {
        if ($ym == "") $ym = date("Y-m");
        return $ym . "-" .self::daysInCurrentMonth($ym);
    }

    /**
     *
     * @param type $ym
     * @return type
     */
    public static function firstDateOfCurrentMonth($ym="")
    {
        if ($ym == "")  $ym = date("Y-m");
        return $ym . "-01";
    }
    
    //["24/06/2016","25/06/2016","26/06/2016"]
    public static function getMultiDatePicker($name,$addDates='["25/06/2016","26/06/2016"]')
    {
        $disableDates = 'addDisabledDates: ["24/06/2016"]';
        $tempid = "md" . rand(0,40000);
        $ret = '<input type="hidden" id="changer" value="changer"/>'
            . '<input type="hidden" name="'.$name.'" id="result'.$tempid.'" />
            <div id="'.$tempid.'" class="box"></div>
            <script>$("#'.$tempid.'").multiDatesPicker({
                   addDisabledDates: ["24/06/2016"]
                });
                $("#changer").change(function(){
                    var dates = $("#'.$tempid.'").multiDatesPicker("getDates");
                    $("#result'.$tempid.'").val(dates);
                });
            </script>';
        return $ret;
    }
}