<?php
namespace tolical;

class ToliCal {
    private $curMonth = null; //yyyy-mm
    private $href = "";

    public function __construct($showMonth=null,$href="") {
        if ($showMonth == null){
            $showMonth = date("Y-m");
        }
        $this->curMonth = substr($showMonth,0,7);
        $this->href= $href;
    }

    public function nextMonth() {
        $this->curMonth = Tolidates::nextMonth($this->curMonth);
    }

    public function prevMonth() {
        $this->curMonth = Tolidates::previousMonth($this->curMonth);
    }

    /**
     * days = "yyyy-mm-dd" or ["yyyy-mm-dd","yyyy-mm-dd",...]
     * color = "blue" or "gray" or "green"
     *         defaults to blue
     * @param type $color
     * @param type $days
     * @return type
     */
    private $grayDays = [];
    private $blueDays = [];
    private $greenDays = [];
    public function markDays($days=[],$color="blue") {
        if (!is_string($color)) { return; }
        if (!is_array($days)){
            $days = [$days];
        }

        switch($color){
            case "green": 
                $this->greenDays = array_merge($this->greenDays,$days);
                break;
            case "gray": 
                $this->grayDays = array_merge($this->grayDays,$days);
                break;
            default:
                $this->blueDays = array_merge($this->blueDays,$days);
                break;
        }
    }

    /**
     *  
     * A day can be put in more than one color-array,
     * Do mind that the colorClass() method takes green before gray, 
     * and gray before blue.  If you need any other prevalence-order
     * 
     * @param type $day
     * @return string
     */
    private function colorClass($day){
        if (in_array($day, $this->greenDays)){
            return "day_green";
        }
        if (in_array($day, $this->grayDays)){
            return "day_gray";
        }
        if (in_array($day, $this->blueDays)){
            return "day_blue";
        }
        return "";
    }

    public function curMonth($newVal="") {
        if ($newVal != ""){
            $this->curMonth = $newVal;
        }
        return $this->curMonth;
    }

    public function href($newVal="") {
        if ($newVal != ""){
            $this->href = $newVal;
        }
        return $this->href;
    }

    public function html($echo=false) {
        $wholeMonth = ToliDates::getDates(
            ToliDates::firstDateOfCurrentMonth($this->curMonth),
            ToliDates::lastDateOfCurrentMonth($this->curMonth)
        );
        $prevMonth = ToliDates::previousMonth($this->curMonth);
        $nextMonth = ToliDates::nextMonth($this->curMonth);
        $monthName = ToliDates::getMonthName($wholeMonth[0]);
        $Year = substr($this->curMonth,0,4);
        $ret = <<<R
            <div class='monthTitle'>
                <a href='{$this->href}/{$prevMonth}-01' >&lt;</a>
                 {$monthName} - {$Year}
                <a href='{$this->href}/{$nextMonth}-01' >&gt;</a>
            </div>
            <div style='clear:both'></div>
            <div class='daysel'>Mo</div>
            <div class='daysel'>Tu</div>
            <div class='daysel'>We</div>
            <div class='daysel'>Th</div>
            <div class='daysel'>Fr</div>
            <div class='daysel'>Sa</div>
            <div class='daysel'>Su</div>
            <div style='clear:both'></div>
R;

        for ($i=1;$i< ToliDates::dateToWeekday($wholeMonth[0]);$i++){
            $ret .= "<div class='emptyday'></div>";
        }
        foreach ($wholeMonth as $d) {
            $day = ToliDates::getDay($d);
            $a = ($this->href == "")? "":"<a href='{$this->href}/{$d}'>";
            $da = ($this->href == "")? "":"</a>";
            $ret .= <<<R
                {$a}<div datum='{$d}' class='day {$this->colorClass($d)}' title='' >{$day}</div>{$da}
R;
            if ( ToliDates::dateToWeekday($d) == 7){
                $ret .= "<div style='clear:both'></div>";
            }
        }
        if ($echo) echo $ret;
        return $ret;
    }
}
